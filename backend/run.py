#! /usr/bin/env python
from app import app
#这里又出现了2个app，Flask应用的实例叫做app，是app包的一员。 from app import app语句导入了app包中的app变量。
# #app = Flask(__name__,
#             static_folder="../../dist/static",
#             template_folder="../../dist")
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5050)